const int red = 52;
const int green = 53;

const int keypad[] = {40, 36, 38, 34, 32, 30, 28, 26, 24, 22};
const int dirs[] = {2, 4, 6, 8, 10};
const int steppers[] = {3, 5, 7, 9, 11};

unsigned long current_time;
unsigned long last_dir_changes[] = {0, 0, 0, 0, 0};
unsigned long last_step_moves[] = {0, 0, 0, 0, 0};

void square_signal_generator(int pin, unsigned long pulse_time, unsigned long &last_visit) {
  // Peut se remplacer par lisibilite avec http://playground.arduino.cc/Code/ElapsedMillis
  if (current_time - last_visit > pulse_time) {
    digitalWrite(pin, !digitalRead(pin));
    last_visit = current_time;
  }
}

void setup() {
  // Initialisation des LED
  pinMode(red, OUTPUT);
  digitalWrite(red, HIGH);
  pinMode(green, OUTPUT);
  digitalWrite(green, LOW);
  
  // Initialisation des floppy + positionnement des chariots
  for (int floppy = 0; floppy < 5; floppy++) {
    pinMode(dirs[floppy], OUTPUT);
    pinMode(steppers[floppy], OUTPUT);
    unsigned long start_cart = micros();
    long unsigned tmp;
    while (micros() < start_cart + 100000) {
      current_time = micros();
      square_signal_generator(steppers[floppy], 1000, tmp);
    }
  }
  
  // Initialisation du clavier
  for (int key = 0; key < 11; key++) {
    pinMode(keypad[key], INPUT);
  }
}

void loop() {
  current_time = micros();
  if (digitalRead(keypad[0]) == LOW) {
    square_signal_generator(2, 300000, last_dir_changes[0]);
    square_signal_generator(3, 8000, last_step_moves[0]);
  }
  if (digitalRead(keypad[1]) == LOW) {
    square_signal_generator(4, 300000, last_dir_changes[1]);
    square_signal_generator(5, 7100, last_step_moves[1]);
  }
  if (digitalRead(keypad[2]) == LOW) {
    square_signal_generator(6, 300000, last_dir_changes[2]);
    square_signal_generator(7, 6350, last_step_moves[2]);
  }
  if (digitalRead(keypad[3]) == LOW) {
    square_signal_generator(8, 300000, last_dir_changes[3]);
    square_signal_generator(9, 6000, last_step_moves[3]);
  }
  if (digitalRead(keypad[4]) == LOW) {
    square_signal_generator(10, 300000, last_dir_changes[4]);
    square_signal_generator(11, 5300, last_step_moves[4]);
  }
  if (digitalRead(keypad[5]) == LOW) {
    square_signal_generator(2, 300000, last_dir_changes[0]);
    square_signal_generator(3, 4700, last_step_moves[0]);
  }
  if (digitalRead(keypad[6]) == LOW) {
    square_signal_generator(4, 300000, last_dir_changes[1]);
    square_signal_generator(5, 4200, last_step_moves[1]);
  }
  if (digitalRead(keypad[7]) == LOW) {
    square_signal_generator(6, 300000, last_dir_changes[2]);
    square_signal_generator(7, 4000, last_step_moves[2]);
  }
  if (digitalRead(keypad[8]) == LOW) {
    square_signal_generator(8, 300000, last_dir_changes[3]);
    square_signal_generator(9, 3500, last_step_moves[3]);
  }
  if (digitalRead(keypad[9]) == LOW) {
    square_signal_generator(10, 300000, last_dir_changes[4]);
    square_signal_generator(11, 3175, last_step_moves[4]);
  }
  if (digitalRead(keypad[0]) == HIGH && digitalRead(keypad[1]) == HIGH && digitalRead(keypad[2]) == HIGH && digitalRead(keypad[3]) == HIGH && digitalRead(keypad[4]) == HIGH && digitalRead(keypad[5]) == HIGH && digitalRead(keypad[6]) == HIGH && digitalRead(keypad[7]) == HIGH && digitalRead(keypad[8]) == HIGH) {
    digitalWrite(green, LOW);
  } else {
    digitalWrite(green, HIGH);
  }
}

